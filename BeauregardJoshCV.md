# Josh Beauregard
## DevOps Engeneer

> [Download PDF](resume.pdf)
> [josh.beaureg@gmail.com](mailto:josh.beaureg@gmail.com)
> (413) 695-3606

------

## Portfolio: {#portfolio}

### Website Applications Contributed To: 

[HMS Systems Biology Department](https://sysbio.med.harvard.edu/), [AIER](https://www.aier.org/), [The Penguin Place](http://penguingiftshop.com/), [Beer Wine Making](http://beer-winemaking.com/), [Speaking of Springfield](http://speakingofspringfield.com/), [Knectar.com](http://www.knectar.com/), [medicinehunter.com](http://medicinehunter.com/) [Riverbend Animal Hospital](http://riverbendanimal.com/) [Jack Daniels](http://jackdaniels.com), [Schools For Children](http://www.schoolsforchildreninc.org/)/[Dearborn Academy](http://www.dearbornacademy.org/)/[Seaport Academy](http://www.seaportacademy.org/)/[Leslie Ellis](http://www.lesleyellis.org/) (Drupal multi Site), [Mass Charter Schools](http://www.masscharterschools.org/), [Mass Energy](https://www.massenergy.org/)/[Rhode Island Power](https://www.ripower.org) (Drupal multi site), [Umass Humanities and Fine Arts department sites](http://www.umass.edu/hfa/academics/departments) (Drupal, Domain Access), [The Global Issue](http://theglobalissue.com/), [Crocker Communications](http://www.crocker.com/), [MIT Deshpande Center](http://deshpande.mit.edu/), [Kavli Institute at Columbia](http://kavli.columbia.edu/)

### Application Servers:

[Ezesoft](http://www.ezesoft.com/), [Cornerstone Software,](http://www.cornerstonesoftware.com/) [Piston](https://www.pistonapp.com/), [Knectar](https://www.knectar.com/), , [Citisoft](http://www.citisoft.com/), [Forsyth Institute](http://forsyth.org/), [Schools For Children Inc](http://www.schoolsforchildreninc.org/), [Mass Energy](https://www.massenergy.org/)/[Rhode Island Power](https://www.ripower.org), [Juxtapid](http://www.juxtapid.com/), Ab Inbev TapWiser app server

### Publications

#### Blog Posts:

* [Selecting default input format filtered text drupal 7 better ux](http://www.knectar.com/labs/selecting default input format filtered text drupal 7 better ux)
* ['linkit target' module released drupal.org](http://www.knectar.com/labs/%E2%80%9Clinkit target%E2%80%9D module released drupalorg) 
* [Streamlining deployments beanstalk](https://www.knectar.com/labs/streamlining deployments beanstalk) 


#### Interviewed on NPR's Market Place

* [http://www.marketplace.org/topics/tech/top-10-ways-avoid-being-tracked-online](http://www.marketplace.org/topics/tech/top-10-ways-avoid-being-tracked-online)

### Code samples:

* [Github account](https://github.com/sanguis)
* [drupal.org profile account](https://drupal.org/user/99947)

------

## Skills {#skills}

### Methodologies:

Agile, Waterfall

### Software:

Nginx, Apache, Chef, Puppet, Behat, Docker, Vagrant, Git, Subversion, Bazaar, CVS, Drupal, WordPress, node.js, Ruby on the Rails, MySQL, Mongo DB.

### Languages:

PHP, Ruby, Shell scripting, HTML,XHTML, HTML5 CSS/SASS/LESS JavaScript, jQuery.

### Operating Systems:

Linux, Mac OS X.x, Windows (cygwin)

### Cloud Vendors:

AWS, Digital Ocean, Rackspace, Linode.

------

## Summary:

DevOps Department Head and creator for the past 4 years. Created tools to make developers' work-flow more efficient by eliminating repetitive tasks. Created test driven development automation software.

Wrote company development guidelines. Tuned applications servers to be in 90th percentile speed wise.Technical educator and liaison between developers and project manages as needed.

Senior Drupal Developer with over 19 years of web application development experience. Experienced in Drupal versions 4.6 - 8 development including custom module development. Hands on experience in every part of the Software Development Life Cycle including: scope building, wire frames, design, theme integration, front and back end, web/mobile application development, server building, tuning and long term administration including down time rescuing. Effective problem solver who works with users and managers to accomplish goals.

## Experience:	

Knectar
: *Technical Director/ Drupal Oriented DevOps/Open source Evangelist*
  __8/11 - 1/15__

  * Developed and maintained client and internal Drupal web sites.
  * Worked with a team of front end developers and project managers to create an HTML5 SASS based start theme that uses all modern semantics.
  * Trained new developers 
  * Provided technical support to project management in areas of product development and site discovery. 
  * Part of team to develop high availability server stack for SASS applications.
  * Developed Drupal back end for an android app for L'Oreal.
  * Technical consultant on the development of the Drupal site jackdaniels.com
  * Designed and Implemented and maintain Highly Available LEMP And MongoDB stack for Pistonapp.com
  * Provided technical guidance to the Drupal developers and project managers of the hms.harvard.edu development team.
  * Lead Developer/DevOps for various Harvard Medical School department sites, all developed using Drupal as a custom platform for development. Built Linux, Nginx PHP MySQL based stacks via Chef provisioning tool for all site to run on.
  * Maintained ongoing dialog and technical assistance/training to internal Drupal developer at Harvard Medical School in areas of custom module development and version control.
  * Administered company production and staging servers (Cent OS, Debian, Ubuntu Linux), via chef scripts and manual builds.
  * Performed Server/Software integration tuning via MySQL, Memcached, APC, Varnish, and Nginx Micro-Caching,
  * Performed internal technical search engine optimization analysis and provided SEO solutions.
  * Wrote technical documentation for clients and in house use of Drupal applications.
  * Led the adoption of the git version control system.
  * Maintained Knectar's presence in the Drupal opensource world, by leading and tracking local Drupal users group via speaking and helping to plan local Drupal Camps (MA, CT and CO).
  * Created/maintain custom hosting stack based in Linux, Nginx, MySQL and PHP
  * Developed Drupal websites. Developed reusable web scraping tools.
  * Prepared software developed for clients in a way that it could be released as open source resources on drupal.org as well as meeting the clients' specific needs.
  * Knerctar is a mostly waterfall oriented shop; interacted with project managers via  Lighthouse, Google Docs, skype and in person

**Left Click Advanced									9/10 - 6/11**

**_Website Developer, System Administrator_**

* Developed and maintained client Drupal web sites, including the install and configuration of Point of Sales software.
* Administered company production and testing servers (Ubuntu, Linux), including maintaining gitosis/Hudson (now Jenkins) based testing server. 
* Performed search engine optimization (SEO). 
* Trained clients how to use web interfaces in various content management systems from the perspective of search engine proficiency.
* Led company in-services on Version Control Systems and Drupal Development.
* Led one on one training with site builders and designers on all aspects of Drupal.
* Participated in AGILE SCRUM meetings in person, via Skype, IRC, and Google Chat.
* Managed Tasks via Redmine and spreadsheets
* Used Photoshop to extract assets and build Drupal themes.

**Sanguis Development 									12/03 - 1/11**

**_Website Developer, Technician, Technical Instruction, Sole Proprietor_**

* Designed, developed, and maintained all client web sites and servers using a variety of software platforms including php/mysql based Drupal, WordPress, Zen Cart, OS Commerce Phorum and custom Ruby on Rails applications as well as other open souce and proprietary software solutions depending on the clients requests and needs.
* Performed regular search engine optimization (SEO) consulting and report authoring.
* Repaired and modified client computers and network.
* Preformed training of clients various content management systems, from a perspective of search engine proficiency.
* Participated in many different formats of client meetings for giving status reports.
* Built custom Drupal 6/Ubercart based CRM, Proposal Generation, Ticketing and Billing system.	 

**Steiger UK ? Youth With A Mission London, England 					3/02 - 5/04**

**_Communications Manager_**

* Developed and maintained web site, adding frequent updates. Made both a static and later a dynamic PHP based version of the site.
* Handled all equerries to organization received via email.
* Setup, maintained, and repaired all computers in the organization.
